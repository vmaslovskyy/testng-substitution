package com.playtika.services;


import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Test;

import com.playtika.model.Suit;
import com.playtika.model.SuitClass;
import com.playtika.model.SuitTest;
import com.playtika.services.SuitClassLoader;
import com.playtika.services.SuitParser;
import com.playtika.services.SuitTestRunner;

public class TestRunnerTest {

	private Suit createSuit(){
		Suit suit = new Suit();
		suit.setName("Suite1");
		
		SuitTest suitTest = new SuitTest();
		suitTest.setName("Regression1");
		
		
		SuitClass suitClass1 = new SuitClass();
		suitClass1.setName("test.Test1");
		suitClass1.setTestClass(SuitClassLoader.load("test.Test1"));
		suitClass1.getIncludedMethods().add("testMethod1");
		
		suitTest.getClasses().add(suitClass1);
		
		SuitClass suitClass2 = new SuitClass();
		suitClass2.setName("test.Test2");
		suitClass2.setTestClass(SuitClassLoader.load("test.Test2"));
		
		suitTest.getClasses().add(suitClass2);
		
		suit.getTests().add(suitTest);
		
		return suit;
		
	}
	
	@Test
	public void testRun() {
		
		Suit suit = createSuit();
		
		SuitTestRunner.run(suit);
		
	}
	
	
	@Test
	public void testRun2() throws FileNotFoundException{
		
		String xmlPath = "src/test/resources/xml/test-suit.xml";
		
		SuitParser parser = new SuitParser();
		Suit suit = parser.getSuit(new FileInputStream(xmlPath));
		
		SuitTestRunner.run(suit);
	}
	

}
