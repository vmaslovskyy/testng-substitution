package com.playtika.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

import com.playtika.model.Suit;
import com.playtika.services.SuitParser;

public class SuitParserTest {
	
	@Test
	public void testGetSuit() throws FileNotFoundException {
		
		String xmlPath = "src/test/resources/xml/test-suit.xml";
		
		SuitParser parser = new SuitParser();
		Suit suit = parser.getSuit(new FileInputStream(xmlPath));
		
		Assert.assertEquals("test.Test1", suit.getTests().get(0).getClasses().get(0).getName());
		Assert.assertEquals("testMethod1", suit.getTests().get(0).getClasses().get(0).getIncludedMethods().get(0));
		Assert.assertEquals("test.Test2", suit.getTests().get(0).getClasses().get(1).getName());
	}
	
}
