package com.playtika.model;

import java.util.ArrayList;
import java.util.List;

public class SuitClass {

	private String name;
	private Class<?> testClass;
	private List<String> excludedMethods = new ArrayList<>();
	private List<String> includedMethods = new ArrayList<>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Class<?> getTestClass() {
		return testClass;
	}
	public void setTestClass(Class<?> testClass) {
		this.testClass = testClass;
	}
	public List<String> getExcludedMethods() {
		return excludedMethods;
	}
	public void setExcludedMethods(List<String> excludedMethods) {
		this.excludedMethods = excludedMethods;
	}
	public List<String> getIncludedMethods() {
		return includedMethods;
	}
	public void setIncludedMethods(List<String> includedMethods) {
		this.includedMethods = includedMethods;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((excludedMethods == null) ? 0 : excludedMethods.hashCode());
		result = prime * result + ((includedMethods == null) ? 0 : includedMethods.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((testClass == null) ? 0 : testClass.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SuitClass other = (SuitClass) obj;
		if (excludedMethods == null) {
			if (other.excludedMethods != null)
				return false;
		} else if (!excludedMethods.equals(other.excludedMethods))
			return false;
		if (includedMethods == null) {
			if (other.includedMethods != null)
				return false;
		} else if (!includedMethods.equals(other.includedMethods))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (testClass == null) {
			if (other.testClass != null)
				return false;
		} else if (!testClass.equals(other.testClass))
			return false;
		return true;
	}

	
	
}
