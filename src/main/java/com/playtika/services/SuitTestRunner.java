package com.playtika.services;

import java.util.List;
import java.util.regex.Pattern;

import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.Request;
import org.junit.runner.Result;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.Failure;

import com.playtika.model.Suit;
import com.playtika.model.SuitClass;
import com.playtika.model.SuitTest;


public class SuitTestRunner {

	public static void run(Suit suit){
		for(SuitTest suitTest : suit.getTests()){
			for(SuitClass suitClass : suitTest.getClasses()){
				runClassTest(suitClass.getTestClass(), suitClass.getIncludedMethods(), suitClass.getExcludedMethods());
			}
		}
	}
	
	public static Result runClassTest(Class<?> testClass, List<String> includeMethods, List<String> excludeMethods){
		 
		JUnitCore core = new JUnitCore();
		core.addListener(new SuitRunListener());
        Request r = Request.aClass(testClass);
        return core.run(r.filterWith(new Filter() {
        	 
        	@Override
        	 public boolean shouldRun(Description description) {
        		 if (description == null) {
                        return false;
                    }
                    if (includeMethods.size() == 0 && excludeMethods.size() == 0) {
                        return true;
                    }
                    for (String m: includeMethods) {
                        Pattern p = Pattern.compile(m);
                        if (p.matcher(description.getMethodName()).matches()) {
                            return true;
                        }
                    }
                    for (String m: excludeMethods) {
                        Pattern p = Pattern.compile(m);
                        if (p.matcher(description.getMethodName()).matches()) {
                            return false;
                        }
                    }
                    return false;
        	 }

			@Override
			public String describe() {
				return "JUnit method filter";
			}
         }));
	}
}

class SuitRunListener extends RunListener {
	
	 @Override
     public void testFailure(Failure failure) throws Exception {
		 System.out.println(failure.getDescription() + " " + failure.getException());
	 }
	 
	 @Override
     public void testAssumptionFailure(Failure failure) {
		 System.out.println(failure.getDescription() + " " + failure.getException());
     }
	 
	 @Override
     public void testFinished(Description description) throws Exception {
		 System.out.println(description.getDisplayName() + " finished");
		 
	 }
	 
	 @Override
     public void testIgnored(Description description) throws Exception {
		 System.out.println(description.getMethodName() + " ignored");
	 }
	 
	 @Override
     public void testRunFinished(Result result) throws Exception {
     }

     @Override
     public void testRunStarted(Description description) throws Exception {
     }

     @Override
     public void testStarted(Description description) throws Exception {
     }
}