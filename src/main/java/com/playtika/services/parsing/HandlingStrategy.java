package com.playtika.services.parsing;

import com.playtika.model.Suit;

public interface HandlingStrategy {
	
	boolean isApplicable(String nodeName);
	
	void processElement(Suit suit, String nodeValue);

}
