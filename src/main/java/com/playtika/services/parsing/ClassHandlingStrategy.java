package com.playtika.services.parsing;

import com.playtika.model.Suit;
import com.playtika.model.SuitClass;
import com.playtika.services.SuitClassLoader;

public class ClassHandlingStrategy implements HandlingStrategy{

	@Override
	public boolean isApplicable(String nodeName) {
		return "class".equals(nodeName);
	}

	@Override
	public void processElement(Suit suit, String nodeValue) {
    	SuitClass suitClass = new SuitClass();
    	suitClass.setName(nodeValue);
    	suitClass.setTestClass(SuitClassLoader.load(nodeValue));
    	suit.getTests().get(suit.getTests().size() - 1).getClasses().add(suitClass);
	}

}
