package com.playtika.services.parsing;

import com.playtika.model.Suit;

public class SuitHandlingStrategy implements HandlingStrategy{

	@Override
	public boolean isApplicable(String nodeName) {
		return "suit".equals(nodeName);
	}

	@Override
	public void processElement(Suit suit, String nodeValue) {
		suit.setName(nodeValue);
	}

}
