package com.playtika.services.parsing;

import java.util.ArrayList;
import java.util.List;

import com.playtika.model.Suit;

public class ElementHandlingFactory {
	
    private static ElementHandlingFactory instance = null;
    private static List<HandlingStrategy> strategies = new ArrayList<>();
    
    private ElementHandlingFactory() {
    	strategies.add(new SuitHandlingStrategy());
    	strategies.add(new TestHandlingStrategy());
    	strategies.add(new ClassHandlingStrategy());
    	strategies.add(new IncludeMethodsHandlingStrategy());
    	strategies.add(new ExcludeMethodsHandlingStrategy());
    }

    public static synchronized ElementHandlingFactory getInstance() {
        if (instance == null)
        	instance = new ElementHandlingFactory();
        return instance;
    }
    
    public void processElement(Suit suit, String nodeName, String nodeValue){
    	for(HandlingStrategy strategy : strategies){
    		if(strategy.isApplicable(nodeName))
    			strategy.processElement(suit, nodeValue);
    	}
    }
    
}
