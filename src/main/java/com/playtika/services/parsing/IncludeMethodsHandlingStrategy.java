package com.playtika.services.parsing;

import java.util.List;

import com.playtika.model.Suit;
import com.playtika.model.SuitClass;

public class IncludeMethodsHandlingStrategy implements HandlingStrategy{

	@Override
	public boolean isApplicable(String nodeName) {
		return "include".equals(nodeName);
	}

	@Override
	public void processElement(Suit suit, String nodeValue) {
		List<SuitClass> classes = suit.getTests().get(suit.getTests().size() - 1).getClasses();
    	suit.getTests().get(suit.getTests().size() - 1).getClasses().get(classes.size() - 1).getIncludedMethods().add(nodeValue);
	}

}
