package com.playtika.services.parsing;

import com.playtika.model.Suit;
import com.playtika.model.SuitTest;

public class TestHandlingStrategy implements HandlingStrategy{

	@Override
	public boolean isApplicable(String nodeName) {
		return "test".equals(nodeName);
	}

	@Override
	public void processElement(Suit suit, String nodeValue) {
		SuitTest suitTest = new SuitTest();
		suitTest.setName(nodeValue);
		suit.getTests().add(suitTest);
	}

}
