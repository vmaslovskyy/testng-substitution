package com.playtika.services;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.playtika.model.Suit;
import com.playtika.services.parsing.ElementHandlingFactory;

public class SuitParser {
	
    public Suit getSuit(InputStream inputStream){
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true); 
        DocumentBuilder builder;
        Suit suit = new Suit();
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(inputStream);
			Element element = (Element) doc.getDocumentElement();
			ElementHandlingFactory.getInstance().processElement(suit, element.getNodeName(), element.getAttribute("name"));
			parse(doc, suit, element);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return suit;
    }
    
    private void parse(final Document doc, Suit suit, final Element e) {
        NodeList children = e.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            final Node node = children.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
            	Element element = (Element) node;
            	ElementHandlingFactory.getInstance().processElement(suit, element.getNodeName(), element.getAttribute("name"));
            	parse(doc, suit, element);
            }
        }
    }
	
}



