package com.playtika.services;

public class SuitClassLoader {

	public static Class<?> load(String className){
		try {
		    return Class.forName(className);
		} catch (ClassNotFoundException e) {
			  e.printStackTrace();
			  return null;
		}
	}
}
